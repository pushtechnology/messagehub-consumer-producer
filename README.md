## Message Hub Producer+Consumer /w Reappt Subscriber

[Reappt.io](https://www.reappt.io) provides web-scale, low latency, realtime, and robust delivery of messages to mobile endpoints.

[Message Hub](http://www-03.ibm.com/software/products/en/ibm-message-hub) is IBM's cloud offering of Kafka. Kafka is fantastic at capturing data but lacks the "Push" capabilities to deliver information in realtime to mobile endpoints.

This demonstration combines two best in class solutions (Reappt and Message Hub) to deliver messages in realtime!

This code publishes and subscribes to MessageHub using the Java Kafka client. The webpage (below) also subscribes to the same topic via Reappt. The bridge between MessageHub and Reappt is described here: https://github.com/pushtechnology/ibm-messagehub

Please retrieve from your MessageHub instance the username, password,
and one of the broker URLs. You can find the configuration from your
**Dashboard > MessageHub Instance > Service Credentials > Credentials-1 >
Click down arrow.**

Please retrieve from your Reappt instance the host, username, and password. You can find the configuration from your
**Dashboard > Reappt Instance > Open Reappt Dashboard > Service Details and System Users**

To build and run:

    mvn clean spring-boot:run \
    	-DmsgHubUser=<USERNAME> -DmsgHubPass=<PASSWORD> \
    	-DmsgHubBroker=<HOST>:<PORT> \
    	-DmsgHubTopic=<TOPIC> \
    	-DreapptUser=<USERNAME> -DreapptPass=<PASSWORD> \
    	-DreapptHost=<HOST>


Wait for the application to launch and begin to display messages from Message Hub...

Then click the following link! [http://localhost:8080/](http://localhost:8080/)

You can access the webapp from any mobile device and see live updates
with low latency for tens of thousands of clients (depending on
your Reappt plan).
