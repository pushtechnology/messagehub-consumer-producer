<html>
<head>
    <title>Reappt Subscriber</title>

    <script type="text/javascript" src="http://developer.pushtechnology.com/clients/js/diffusion.js"></script>
</head>
<body>
<div style="font-family: 'DejaVu Sans', Arial, Helvetica, sans-serif">
    <span>The value of ?Kafka/outbound/.*: </span> <br />
    <div id="update" style="margin-left:30px"></div>
</div>

<script type="text/javascript">
    function subscribeToTopic(session) {
        session.subscribe('?Kafka/outbound/.*') <!-- subscribe to all topics under Kafka/outbound/ -->
                .on('update', function(data, topic) {
                            var dobj = JSON.parse(data);
                            if ( typeof dobj.SessionEvent !== "undefined" ) {
                                // Ignore session events
                            }
                            else {
                                document.getElementById('update').innerHTML += topic+": "+data+"<br />";
                            }
                        }
                );
    }
    diffusion.connect({
        // Please supply these parameters via the command line when launching
        // the sample application. See README.md for full details
        host        : '<%=System.getProperty("reapptHost")%>',
        port        : 443,
        secure      : true,
        principal   : '<%=System.getProperty("reapptUser")%>',
        credentials : '<%=System.getProperty("reapptPass")%>'
    }).then(subscribeToTopic);
</script>
</body>
</html>
