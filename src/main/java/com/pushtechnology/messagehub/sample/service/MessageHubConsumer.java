/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pushtechnology.messagehub.sample.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Properties;


/**
 * Simple Message Hub / Kafka consumer to demonstrate
 * information flow through Message Hub
 */
public class MessageHubConsumer implements Runnable {
    private static final Logger logger = Logger.getLogger ( MessageHubConsumer.class );
    private KafkaConsumer<byte[], byte[]> k_consumer;
    private boolean closing;

    /**
     * Constructor to configure and start a Kafka Consumer.
     * Properties are documented here: <a href="http://kafka.apache.org/documentation.html#consumerconfigs">here</a>
     * @param props Properties (see link)
     * @param topic The topic to subscribe
     */
    public MessageHubConsumer(Properties props, String topic) {
        closing = false;

        logger.log ( Level.DEBUG, "Starting Message Hub Consumer Java Sample" );
        k_consumer = new KafkaConsumer<> ( props );
        k_consumer.subscribe ( Collections.singletonList(topic) );
    }

    /**
     * Receives and displays changes to the topic.
     * Kafka requires this client to poll as it doesn't
     * support "push" delivery of information.
     */
    @Override
    public void run () {

        while (!closing) {
            try {
                // Poll on the Kafka consumer every second.
                // Iterate through all the messages received and print their content.
                // After a predefined number of messages has been received, the client will exit.
                for ( ConsumerRecord<byte[], byte[]> record : k_consumer.poll(1000) ) {
                    final String message = new String(record.value(), Charset.forName("UTF-8"));
                    logger.log(Level.INFO, "Message: " + message);
                }

                k_consumer.commitSync ();

                Thread.sleep ( 1000 );

            } catch (final InterruptedException e) {
                // Spurious wakeup, keep on ticking
            } catch (final Exception e) {
                logger.log ( Level.ERROR, "Consumer has failed with exception: " + e );
                shutdown();
            }
        }
    }

    /**
     * Notifies the worker to shutdown
     */
    public void shutdown () {
        closing = true;
    }
}
