/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pushtechnology.messagehub.sample.service;

import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import java.util.Map;

/**
 * Class created to enable username and password to be passed via environment variables
 * instead of being committed to SCM in the jaas.conf file.
 *
 *  -DmsgHubUser=username
 *  -DmsgHubPass=password
 *
 */
@SuppressWarnings("unused")
public class MessageHubLoginModule extends com.ibm.messagehub.login.MessageHubLoginModule {
    /**
     * Fetch properties from system environment variables before using
     * the configuration file. This prevents secure details being committed
     * to SCM.
     *
     *  -DmsgHubUser=username
     *  -DmsgHubPass=password
     *
     * @param subject
     * @param callbackHandler
     * @param sharedState
     * @param options
     */
    public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState, Map<String, ?> options) {
        String username = System.getProperty("msgHubUser");
        String password = System.getProperty("msgHubPass");

        if ( null == username) {
            username = (String)options.get("username");
        }

        if ( null == password) {
            password = (String)options.get("password");
        }

        this.setUserName(subject, username);
        this.setPassword(subject, password);
    }

}
