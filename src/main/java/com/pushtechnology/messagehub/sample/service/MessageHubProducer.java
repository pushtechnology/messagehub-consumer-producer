/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pushtechnology.messagehub.sample.service;

import com.pushtechnology.messagehub.sample.service.util.MessageList;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.Properties;

/**
 * Simple Message Hub / Kafka producer to demonstrate
 * information flow through Message Hub
 */
public class MessageHubProducer implements Runnable {
    private static final Logger logger = Logger.getLogger(MessageHubProducer.class);
    private KafkaProducer<byte[], byte[]> k_producer;
    private boolean closing;
    private String topic;
    private int producedMessages = 0;

    /**
     * Constructor to configure and start a Kafka Producer.
     * Properties are documented here: <a href="http://kafka.apache.org/documentation.html#producerconfigs">here</a>
     * @param props Properties (see link)
     * @param topic The topic to receive published data
     */
    public MessageHubProducer(Properties props, String topic) {
        closing = false;

        logger.log(Level.DEBUG, "Starting Message Hub Consumer Java Sample");
        this.k_producer = new KafkaProducer<>(props);

        this.topic = topic;
    }

    /**
     * Publishes a test message every second
     */
    @Override
    public void run () {

        while (!closing) {
            String fieldName = "records";
            // Push a message into the list to be sent.
            MessageList list = new MessageList();
            list.push("This is a test message " + producedMessages);

            try {
                // Create a producer record which will be sent
                // to the Message Hub service, providing the topic
                // name, field name and message. The field name and
                // message are converted to UTF-8.
                ProducerRecord<byte[], byte[]> record =
                        new ProducerRecord<>( topic,
                                              fieldName.getBytes("UTF-8"),
                                              list.toString().getBytes("UTF-8") );

                // Synchronously wait for a response from Message Hub / Kafka.
                RecordMetadata m = k_producer.send(record).get();
                producedMessages++;

                logger.log(Level.INFO, "Message produced, offset: " + m.offset());

                Thread.sleep(1000);
            } catch (final InterruptedException ie) {
                // Spurious wakeup, keep on ticking
            } catch (final Exception e) {
                logger.log ( Level.ERROR, "Producer has failed with exception: " + e );
                shutdown();
            }
        }

    }

    /**
     * This notifies the worker thread to end
     */
    public void shutdown () {
        closing = true;
    }
}
