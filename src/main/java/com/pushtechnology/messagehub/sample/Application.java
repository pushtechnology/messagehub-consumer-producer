/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pushtechnology.messagehub.sample;

import com.pushtechnology.messagehub.sample.service.MessageHubConsumer;
import com.pushtechnology.messagehub.sample.service.MessageHubProducer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

@Configuration
@EnableAutoConfiguration
@ComponentScan
public class Application {
	private static Logger logger = Logger.getLogger( Application.class );
	private static final String JAAS_CONFIG_PROPERTY = "java.security.auth.login.config";

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);

		startMessageHubProducerConsumer();
	}

	/**
	 * Start the Message Hub Producer and Consumer.
	 * The Reappt consumer is a web page, visit <a href="http://localhost:8080/">localhost:8080</a>
	 *
	 */
	private static void startMessageHubProducerConsumer() throws InterruptedException, ExecutionException, IOException {
		Thread consumerThread, producerThread;

		if (System.getProperty(JAAS_CONFIG_PROPERTY) == null) {
            URL jaasFile = Application.class.getClassLoader().getResource("jaas.conf");
            if ( null == jaasFile ) {
                logger.error("jaas.conf file cannot be found on classpath, please rebuild the full project");
            }
            else {
                System.setProperty(JAAS_CONFIG_PROPERTY, jaasFile.toString());
            }
		}

		logger.log(Level.INFO, "Using JAAS "+System.getProperty(JAAS_CONFIG_PROPERTY));

		String topic  = System.getProperty("msgHubTopic");
		String broker = System.getProperty("msgHubBroker");

		if ( null == topic ) {
			logger.error("The system environment variable -DmsgHubTopic=<TOPIC> is not defined");
            return;
		}

		if ( null == broker ) {
			logger.error("The system environment variable -DmsgHubBroker=<BROKER> is not defined");
            return;
		}

		Properties props = new Properties ();

		props.put("acks",               "-1");
		props.put("auto.offset.reset",  "latest" );
		props.put("bootstrap.servers",  broker);
		props.put("client.id",          "Reappt-message-hub-example");
		props.put("enable.auto.commit", "false" );
		props.put("group.id",           "reappt");

		props.put("key.serializer",     "org.apache.kafka.common.serialization.ByteArraySerializer");
		props.put("key.deserializer",   "org.apache.kafka.common.serialization.ByteArrayDeserializer" );
		props.put("value.serializer",   "org.apache.kafka.common.serialization.ByteArraySerializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer" );

		props.put("sasl.mechanism",     "PLAIN");
		props.put("security.protocol",  "SASL_SSL");
		props.put("ssl.protocol",       "TLSv1.2");
		props.put("ssl.enabled.protocols", "TLSv1.2");
		props.put("ssl.endpoint.identification.algorithm", "HTTPS");


		consumerThread = new Thread(new MessageHubConsumer(props, topic));
		producerThread = new Thread(new MessageHubProducer(props, topic));

		// Start threads!
		consumerThread.start();
		producerThread.start();
	}
}
